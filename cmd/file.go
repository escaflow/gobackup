package cmd

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/escaflow/gobackup/conf"
	"gitlab.com/escaflow/gobackup/file"
)

// fileCmd represents the file command
var fileCmd = &cobra.Command{
	Use:   "file",
	Short: "Backups a single file to a given destination",
	Long: `For example:

Backup /opt/folder/myfile to /mnt/backup/myfile.tar.bz2
and keep 7 copys
gobackup -p /opt/folder/myfile -b /mnt/backup/ -n myfile -k 7`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := startFileBackup(config); err != nil {
			fmt.Println("[Error] File Backup failed", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(fileCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// fileCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// fileCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	fileCmd.Flags().StringVarP(&config.SourcePath, "source", "s", "", "Source Path (required)")
	fileCmd.MarkFlagRequired("path")
	fileCmd.Flags().StringVarP(&config.TargetPath, "backup", "b", "", "Backup files to Path")
	fileCmd.MarkFlagRequired("backup")
	fileCmd.Flags().StringVarP(&config.BackupName, "name", "n", "", "Backup Name (default to File/Dir name)")
	fileCmd.Flags().IntVarP(&config.Time, "time", "t", 0, "Backup files older than time")
	fileCmd.Flags().BoolVarP(&config.Remove, "remove", "r", false, "Remove backuped files")
	fileCmd.Flags().IntVarP(&config.KeepCount, "keep", "k", 0, "Backup rotation count")
	fileCmd.Flags().IntVarP(&config.KeepDay, "day", "d", 0, "Backup rotation in days")
}

func startFileBackup(c conf.File) (err error) {
	startTime := time.Now()
	fmt.Printf("[INFO]: Starting File type Backup\n")
	if err := file.Main(config); err != nil {
		fmt.Println("[Error] File Backup failed", err)
	}
	// duration := time.Duration(80) * time.Second
	// time.Sleep(duration)
	// Print out runtime, we can either use a var to build the diff in int or just put it inside the printf
	durationMinute := int(time.Now().Sub(startTime).Minutes())
	fmt.Printf("[INFO]: Ending File type Backup in %v minutes or %v seconds\n", durationMinute, int(time.Now().Sub(startTime).Seconds()))
	return err
}
