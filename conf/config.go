package conf

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

// File structure
type File struct {
	SourcePath  string
	Time        int
	TargetPath  string
	BackupName  string
	ArchivePath string
	Remove      bool
	KeepCount   int
	KeepDay     int
}

var pathError *os.PathError
var archInfo os.FileInfo
var err error
var rand uint32
var randmu sync.Mutex

// CheckConfig checks the given configuration for errors
func CheckConfig(config File) (err error) {
	// conf.keepCount || conf.keepDay
	if config.KeepCount > 0 && config.KeepDay > 0 {
		fmt.Println("[ERROR]: Use either Days or count rotation")
		fmt.Println("[DEBUG]: config.KeepCount:", config.KeepCount)
		fmt.Println("[DEBUG]: config.KeepDay:", config.KeepDay)
		os.Exit(1)
	}

	// config.targetPath
	// we can check it here or later if we want do asess the ArchivePath...
	// if CheckPathCreate(config.targetPath) {
	// 	os.Exit(1)
	// }

	// config.sourcePath
	if !CheckPath(config.SourcePath) {
		os.Exit(1)
	}
	if _, err := os.Stat(config.SourcePath); err != nil {
		fmt.Println("[DEBUG]: config.SourcePath:", config.SourcePath)
		if errors.As(err, &pathError) {
			fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
			os.Exit(1)
		}
	}

	return
}

// GetArchiveName returns the name of the archive to use
func GetArchiveName(config File) (backupName string, err error) {
	// if we have no backup name, we need to construct it
	if len(config.BackupName) == 0 {
		// if we have a / at the end we want to trim it first, then set the backup name to the sourcepath dir
		if strings.HasSuffix(config.SourcePath, "/") {
			config.SourcePath = strings.TrimRight(config.SourcePath, "/")
			// fmt.Printf("[DEBUG]: Last Word: %v\n", config.SourcePath[strings.LastIndex(config.SourcePath, "/")+1:len(config.SourcePath)])
			backupName = config.SourcePath[strings.LastIndex(config.SourcePath, "/")+1 : len(config.SourcePath)]
		} else {
			// fmt.Printf("[DEBUG]: Last Word: %v\n", config.SourcePath[strings.LastIndex(config.SourcePath, "/")+1:len(config.SourcePath)])
			backupName = config.SourcePath[strings.LastIndex(config.SourcePath, "/")+1 : len(config.SourcePath)]
		}
	} else {
		backupName = config.BackupName
	}
	return
}

// CheckBackupName checks if the target name is avaible
func CheckBackupName(config File) (archivePath string, success bool) {
	config.ArchivePath, err = filepath.Abs(filepath.Join(config.TargetPath, config.BackupName, fmt.Sprintf("%s-%s", config.BackupName, time.Now().Format("02-01-2006"))))
	if err != nil {
		success = false
		return
	}
	fullTestPath := fmt.Sprintf("%s.tar.gz", config.ArchivePath)
	// can we use the targetPath?
	// fmt.Printf("[DEBUG]: checking: %s\n", config.ArchivePath)
	if CheckPathCreate(config.TargetPath) {
		// is there already a backup with the given name?
		// fmt.Printf("[DEBUG]: checking: %s\n", fullTestPath)
		if _, err := os.Stat(fullTestPath); os.IsNotExist(err) {
			archivePath = config.ArchivePath
			success = true
			// return
		}
		// try add a timestamp so we dont overwrite the other one, it isn't perfect but better then nothing
		config.ArchivePath = fmt.Sprintf("%s-%s", config.ArchivePath, time.Now().Format("15:04:05"))
		fullTestPath := fmt.Sprintf("%s.tar.gz", config.ArchivePath)
		// fmt.Printf("[DEBUG]: checking: %s\n", fullTestPath)
		if _, err := os.Stat(fullTestPath); os.IsNotExist(err) {
			archivePath = config.ArchivePath
			success = true
			return
		}
		// i wonder when we get here, maybe if someone spams symlinks with timestamps?
		// fmt.Printf("[DEBUG]: config.ArchivePath %s is already taken\n", fullTestPath)
		success = false
		return

	}
	success = false
	return
}

// CheckPath checks if the path exist
func CheckPath(path string) (exist bool) {
	if _, err := os.Stat(path); err != nil {
		if errors.As(err, &pathError) {
			fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
			exist = false
		} else {
			exist = false
			println(err)
		}
	} else {
		exist = true
	}

	return
}

// CheckPathCreate checks if the path exist, if not try to create it
func CheckPathCreate(path string) (success bool) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		// try to create the target dir
		if err := os.MkdirAll(path, 0770); err != nil {
			if errors.As(err, &pathError) {
				fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
				success = false
				return
			}
		}
	}
	success = true
	return
}

// DeleteFile rm file
func DeleteFile(path string) {
	// delete file
	var err = os.Remove(path)
	if errors.As(err, &pathError) {
		fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
	}
	fmt.Printf("[DEBUG]: File Deleted %s\n", path)
}

// CreateFile just like touch
func CreateFile(path string) (success bool) {
	// check if file exists
	var _, err = os.Stat(path)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if errors.As(err, &pathError) {
			fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
		}
		defer file.Close()
	}
	success = true
	fmt.Printf("[DEBUG]: File Created %s", path)
	return
}
