package file

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/escaflow/gobackup/conf"
)

var targetFiles []string
var err error
var pathError *os.PathError
var success bool

// Main for file backup stuff
func Main(c conf.File) (err error) {
	if err := conf.CheckConfig(c); err != nil {
		// fmt.Println(err)
		// os.Exit(1)
	}

	if c.Time > 0 {
		// fill the targetFiles array with files older then c.Time
		targetFiles, err = getFilesOlderThen(c.SourcePath, c.Time)
		if err != nil {
			fmt.Println(err)
		}
	} else {
		// fill the targetFiles array with all files
		targetFiles, err = getFiles(c.SourcePath)
		if err != nil {
			fmt.Println(err)
		}
	}

	if len(targetFiles) > 0 {
		fmt.Printf("[INFO]: Found %d eligible files inside %s\n", len(targetFiles), c.SourcePath)
		c.BackupName, _ = conf.GetArchiveName(c)
		// fmt.Printf("[DEBUG]: BackupName is %s\n", c.BackupName)
		c.ArchivePath, success = conf.CheckBackupName(c)
		if !success {
			fmt.Printf("[ERROR]: could not get a usable backup path\n")
			os.Exit(1)
		}
		backupDir, _ := filepath.Abs(filepath.Join(c.TargetPath, c.BackupName))
		if !conf.CheckPathCreate(backupDir) {
			fmt.Printf("[ERROR]: could not Create Path %s", backupDir)
			os.Exit(1)
		}
		// we may want to implement other compression methods later
		err := tarFiles(targetFiles, c.SourcePath, fmt.Sprintf("%s.tar", c.ArchivePath))
		if err != nil {
			fmt.Println(err)
			return err
		}
		err = gzipFile(fmt.Sprintf("%s.tar", c.ArchivePath), fmt.Sprintf("%s.tar.gz", c.ArchivePath))
		if err != nil {
			fmt.Println(err)
			return err
		}
		fmt.Printf("[INFO]: Backup complete %s.tar.gz\n", c.ArchivePath)
		if c.KeepDay > 0 {
			fmt.Printf("[DEBUG]: Cleanup path %s\n", backupDir)
			// rotateByDay(backupDir, c.KeepDay)
		}
		if c.KeepCount > 0 {
			fmt.Printf("[DEBUG]: Cleanup path %s\n", backupDir)
			rotateByCount(backupDir, c.KeepCount)
		}
	} else {
		fmt.Printf("[INFO]: Found %d eligible files inside %s\n", len(targetFiles), c.SourcePath)
	}

	return err
}

// TODO: error handling for files we cant read
func getFiles(dir string) ([]string, error) {
	var files []string
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})
	return files, err
}

// TODO: error handling for files we cant read
func getFilesOlderThen(dir string, days int) ([]string, error) {
	var files []string
	err := filepath.Walk(dir, func(path string, file os.FileInfo, err error) error {
		if !file.IsDir() && fileOlderThan(file.ModTime(), days) {
			files = append(files, path)
			// println("[DEBUG]: File:" + path)
		}
		return nil
	})
	return files, err
}

func fileOlderThan(t time.Time, days int) bool {
	// get diff from time.Time and time.Time
	// timeDiff := time.Now().Sub(t)
	// cast time.Duration into int and compare it with days
	if days < int(time.Now().Sub(t).Hours()/24) {
		// fmt.Printf("[DEBUG]: In range, Diffrence: %d Days\n", int(timeDiff.Hours()/24))
		return true
	}
	return false
	//  else {
	// 	// fmt.Printf("[DEBUG]: Out of range, Diffrence: %d Days\n", int(timeDiff.Hours()/24))
	// 	return false
	// }

}

func tarFiles(targetFiles []string, baseDir string, tarPath string) (err error) {
	// we need the file pointer
	tarfile, err := os.Create(tarPath)
	if errors.As(err, &pathError) {
		fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
	}
	defer tarfile.Close()
	// now we construct our io.Writer
	tarball := tar.NewWriter(tarfile)
	defer tarball.Close()
	// iterate trough every file and write it into the tar
	for _, filePath := range targetFiles {
		// get the fileinfo from the files we want to tar
		fileInfo, err := os.Stat(filePath)
		if errors.As(err, &pathError) {
			fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
		}
		// we want to get the file header
		header, err := tar.FileInfoHeader(fileInfo, fileInfo.Name())
		if err != nil {
			return err
		}
		// rewrite the header to remove the prefix path
		header.Name = strings.TrimPrefix(filePath, baseDir)

		if err := tarball.WriteHeader(header); err != nil {
			return err
		}
		file, err := os.Open(filePath)
		if err != nil {
			return err
		}
		defer file.Close()
		// finally copy the file inside the tar
		_, err = io.Copy(tarball, file)

	}
	return
}

func gzipFile(source, target string) error {
	// fmt.Printf("[DEBUG]: source gzip is %s\n", source)
	// fmt.Printf("[DEBUG]: target gzip is %s\n", target)
	reader, err := os.Open(source)
	if errors.As(err, &pathError) {
		fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
	}

	writer, err := os.Create(target)
	if errors.As(err, &pathError) {
		fmt.Println("[ERROR]: " + pathError.Op + " " + pathError.Path + ": " + pathError.Err.Error())
	}
	defer writer.Close()

	archiver := gzip.NewWriter(writer)

	archiver.Name = target
	defer archiver.Close()

	_, err = io.Copy(archiver, reader)
	if err == nil {
		conf.DeleteFile(source)
	}
	return err
}

// TODO: handle files in folders below the backupDir
func rotateByDay(path string, days int) {
	// this one is simple, just call our function and iterate over the array
	oldBackups, _ := getFilesOlderThen(path, days)
	for _, filePath := range oldBackups {
		conf.DeleteFile(filePath)
	}
}

// TODO: handle files in folders below the backupDir
func rotateByCount(path string, count int) {
	// first get all files inside the folder
	oldBackups, _ := getFiles(path)
	// now we iterate backwards over the array
	for index := len(oldBackups) - 1; index >= 0; index-- {
		// if we hit the "top" array, subtract the cont and delete the result
		if index < len(oldBackups)-count {
			conf.DeleteFile(oldBackups[index])
		}
	}

}
